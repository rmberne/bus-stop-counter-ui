import React, { useEffect, useState } from 'react';
import { BusLine } from './BusLine';
import { getTopLines } from '../api/BusStopCounterAPI';

export const TopLinesWithMoreStops = (props) => {
  const [topLines, setTopLines] = useState([]);
  useEffect(() => {
    const init = async () => {
      const data = await getTopLines(props.top);
      setTopLines(data);
    };
    init();
  }, [setTopLines]);

  return (
    <div className="line-content" data-testid="topLinesList">
      {topLines.map(line => <BusLine key={line.number} line={line}/>)}
    </div>
  );
};