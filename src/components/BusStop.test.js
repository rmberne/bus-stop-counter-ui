import React from 'react';
import { render } from '@testing-library/react';
import { BusStop } from './BusStop';

describe('BusStop component', () => {
  it('should render the Bus stop', () => {
    const stopData = {
      number: '001',
      name: 'Stop 01',
    };
    const { getByText } = render(<BusStop stop={stopData}/>);
    const numberDivElement = getByText(/^001$/i);
    const nameDivElement = getByText(/^Stop 01$/i);

    expect(numberDivElement).toBeInTheDocument();
    expect(nameDivElement).toBeInTheDocument();
  });
});
