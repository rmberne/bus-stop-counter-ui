import React from 'react';
import { act, fireEvent, render, waitForElement } from '@testing-library/react';
import { BusLine } from './BusLine';

const mockFetch = response => jest.fn().mockImplementation(() => Promise.resolve(response));

const lineData = {
  number: '001',
  numberOfStops: 2,
};

describe('BusLine component', () => {
  it('should render the Bus line', () => {
    const { getByText } = render(<BusLine line={lineData}/>);
    const numberTextDivElement = getByText(/^Line$/);
    const numberValueDivElement = getByText(/^001$/i);
    const numberOfStopsTextDivElement = getByText(/^stops$/i);
    const numberOfStopsValueDivElement = getByText(/^2$/i);

    expect(numberTextDivElement).toBeInTheDocument();
    expect(numberValueDivElement).toBeInTheDocument();
    expect(numberOfStopsTextDivElement).toBeInTheDocument();
    expect(numberOfStopsValueDivElement).toBeInTheDocument();
  });
  it('should load stops on click', async () => {
    fetch = mockFetch({ ok: true, json: () => [] });
    await act(async () => {
      const { getByTestId } = render(<BusLine line={lineData}/>);
      const lineContentBlockDivElement = getByTestId('lineContentBlock_001');

      fireEvent.click(lineContentBlockDivElement);

      expect(fetch).toHaveBeenCalledWith('api/v1/bus-stops-counter/lines/001/stops');
    });
  });
  it('should show stops', async () => {
    fetch = mockFetch({
      ok: true,
      json: () => [{ number: '1001', name: 'Stop name 1001' }],
    });
    await act(async () => {
      const wrapper = render(<BusLine line={lineData}/>);
      const lineContentBlockDivElement = wrapper.getByTestId('lineContentBlock_001');

      fireEvent.click(lineContentBlockDivElement);

      const stopContentBlockDivElement = await waitForElement(() => wrapper.getByTestId('stopContentBlock_1001'));

      expect(stopContentBlockDivElement).toBeInTheDocument();
    });
  });
});
