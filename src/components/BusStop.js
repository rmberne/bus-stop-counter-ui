import React from 'react';
import './BusStop.css';

export const BusStop = (props) => (
  <div className="stop-container" data-testid={`stopContentBlock_${props.stop.number}`}>
    <div className="stop-item">
      <div className="stop-item-number">{props.stop.number}</div>
      <div className="stop-item-name">{props.stop.name}</div>
    </div>
  </div>
);
