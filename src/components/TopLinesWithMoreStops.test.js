import React from 'react';
import { act, render, waitForElement } from '@testing-library/react';
import { TopLinesWithMoreStops } from './TopLinesWithMoreStops';

const mockFetch = response => jest.fn().mockImplementation(() => Promise.resolve(response));

describe('TopLinesWithMoreStops component', () => {
  it('should render bus lines', async () => {
    fetch = mockFetch({
      ok: true, json: () => [{ number: '001', numberOfStops: 0 }, { number: '002', numberOfStops: 0 }],
    });
    await act(async () => {
      const { getByTestId } = render(<TopLinesWithMoreStops top="2"/>);

      const lineContentBlock1 = await waitForElement(() => getByTestId('lineContentBlock_001'));
      const lineContentBlock2 = await waitForElement(() => getByTestId('lineContentBlock_002'));

      expect(lineContentBlock1).toBeInTheDocument();
      expect(lineContentBlock2).toBeInTheDocument();
    });
  });
});
