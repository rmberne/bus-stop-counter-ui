import React, { useState } from 'react';
import './BusLine.css';
import { BusStop } from './BusStop';
import { getStops } from '../api/BusStopCounterAPI';

export const BusLine = (props) => {
  const [stops, setStops] = useState([]);
  const [showStops, setShowStops] = useState(false);

  const toggleShowStops = async (number) => {
    if (stops.length === 0) {
      setStops(await getStops(number));
    }
    setShowStops(!showStops);
  };

  const renderStops = stops.map(stop => <BusStop key={stop.number} stop={stop}/>);

  return (
    <div className="line-content" onClick={() => toggleShowStops(props.line.number)}
         data-testid={`lineContentBlock_${props.line.number}`}>
      <div className="line-item">
        <div className="line-item-number">
          <div>Line</div>
          <div>{props.line.number}</div>
        </div>
        <div className="line-item-num-stops">
          <div>stops</div>
          <div>{props.line.numberOfStops}</div>
        </div>
        {showStops
          ? <div className="line-stop-wrapper">{renderStops}</div>
          : null
        }
      </div>
    </div>
  );
};
