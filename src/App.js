import React from 'react';
import './App.css';
import { TopLinesWithMoreStops } from './components/TopLinesWithMoreStops';

function App() {
  return (
    <div className="App">
      <header className="App-header" data-testid="header">
        <h1>Bus Stop Count</h1>
      </header>
      <TopLinesWithMoreStops top="10"/>
    </div>
  );
}

export default App;
