import { getStops, getTopLines } from './BusStopCounterAPI';

const mockFetch = response => jest.fn().mockImplementation(() => Promise.resolve(response));

describe('BusStopCounterAPI', () => {
  describe('getTopLines()', () => {
    it('get top lines should return json data', async () => {
      const expected = [
        {
          number: '001',
          numberOfStops: 100,
        },
        {
          number: '002',
          numberOfStops: 99,
        },
        {
          number: '003',
          numberOfStops: 98,
        },
        {
          number: '004',
          numberOfStops: 97,
        },
        {
          number: '005',
          numberOfStops: 96,
        },
      ];
      fetch = mockFetch({
        ok: true,
        json: () => expected,
      });
      const data = await getTopLines(10);

      expect(data).toBe(expected);
    });
    it('get top lines should throw an error', async () => {
      fetch = mockFetch({ ok: false });
      expect(getTopLines(10)).rejects.toMatch('Failed to get top lines');
    });
  });
  describe('getStops()', () => {
    it('get stops should return json data', async () => {
      const expected = [
        {
          number: '001',
          name: 'Sveaväegen',
        },
        {
          number: '002',
          name: 'Grev Turegatan',
        },
      ];
      fetch = mockFetch({
        ok: true,
        json: () => expected,
      });
      const data = await getStops('000');

      expect(data).toBe(expected);
    });
    it('get stops should throw an error', async () => {
      fetch = mockFetch({ ok: false });
      expect(getStops('any')).rejects.toMatch('Failed to get stops');
    });
  });
});