export const getTopLines = async (top) => {
  const response = await fetch(`api/v1/bus-stops-counter/lines?top=${top}`);
  if (response.ok) {
    return await response.json();
  }
  throw new Error('Failed to get top lines');
};

export const getStops = async (number) => {
  const response = await fetch(`api/v1/bus-stops-counter/lines/${number}/stops`);
  if (response.ok) {
    return await response.json();
  }
  throw new Error(`Failed to get stops for line ${number}`);
};
