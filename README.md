# Bus Stop Counter UI #

This is an application to display which bus lines that have the most bus stops
on their route.

It uses the backend in [bus-stop-counter](https://bitbucket.org/rmberne/bus-stop-counter/).
Make sure you are running the backend before starting the frontend.

## Setup

Make sure you have installed:

1. NodeJS
2. yarn

## Commands

### `yarn start`

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test`

Launches the test runner in the interactive watch mode.
